#github-interrogation.py
#20171127

''' github_interrogation.py
Reads data from the Github API and outputs into a new .txt file.

Usage:
    github_interrogation.py [--login_usr=<val>] [--psswrd=<val>] [--search_root=<val>]
    github_interrogation.py -h | --help
    github_interrogation.py --version

Options:
	--login_usr=<val>		username for Github login [default: JoWhitt]
	--psswrd=<val>			password for Github login [default: t3mppassw0rd]
	--search_root=<val>		user name for the root user of the search [default: McGizzle]
	--version
'''

import docopt
from github import Github
import csv

MAX_SEARCH_DEPTH = 3
HOURS_PER_DAY = 24
EVENTS_PER_HOUR = [0,0,0,0,0,0,
					0,0,0,0,0,0,
					0,0,0,0,0,0,
					0,0,0,0,0,0]
DEFAULT_LOGIN = ['JoWhitt', 't3mppassw0rd']
DEFAULT_SEARCH_ROOT = 'McGizzle'

#### FUNCTION DEFINITIONS ####
def countEvents(user):
	for event in user.get_events():
		EVENTS_PER_HOUR[event.created_at.hour] += 1
		
def countEventsOfFollowers(user, currentDepth):
	countEvents(user)
	if(currentDepth < MAX_SEARCH_DEPTH):
		for follower in user.get_followers():
			countEventsOfFollowers(follower, currentDepth+1)
			
def printEventTimeCounts():
	print ('Events per hour:')
	for time in range(0, HOURS_PER_DAY):
		print (time, EVENTS_PER_HOUR[time])
	
def main():	
	arguments = docopt.docopt(__doc__)
	login_usr = arguments['--login_usr']
	login_psswrd = arguments['--psswrd']
	search_root = arguments['--search_root']

	if login_usr==None: login_usr=DEFAULT_LOGIN[0]
	if login_psswrd==None: login_psswrd=DEFAULT_LOGIN[1]
	if search_root==None: search_root=DEFAULT_SEARCH_ROOT

	# LOG ONTO GITHUB
	gh = Github(login_usr, login_psswrd)
	print ("Logged onto Github as", gh.get_user().name)

	# GET SEARCH ROOT
	root_user = gh.get_user(login=search_root)
	print ("Search root:", root_user.name, root_user.id,
		"\nMax search depth:", MAX_SEARCH_DEPTH)
			
	# RECURSIVELY COUNT EVENTS
	print ('Recursively counting events per hour...')
	countEventsOfFollowers(root_user, 0)
	print ('Events per hour:\n', EVENTS_PER_HOUR)
		
	# SET UP & POPULATE CSV FILE
	with open('eventCounts.csv', 'w') as csvfile:
		writer = csv.writer(csvfile)
		for time in range(0, HOURS_PER_DAY):
			writer.writerow([str(EVENTS_PER_HOUR[time])])
	csvfile.close()

main()
			
	
	
	
	
	
	
	
	
	