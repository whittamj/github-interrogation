#graph_data.py
#20171213

''' graph_data.py

Reads data from .csv and outputs a graphical representation.

Usage:
    graph_data.py <filename>
    graph_data.py -h | --help
    graph_data.py --version

Options:
	--version

'''

import docopt
import csv

HOURS_PER_DAY = 24
EVENT_TIME_COUNTS = [0,0,0,0,0,0,
					0,0,0,0,0,0,
					0,0,0,0,0,0,
					0,0,0,0,0,0]
TIMES = ['00:00','01:00','02:00','03:00','04:00','05:00',
		'06:00','07:00','08:00','09:00','10:00','11:00',
		'12:00','13:00','14:00','15:00','16:00','17:00',
		'18:00','19:00','20:00','21:00','22:00','23:00']
					
#### MAIN ####################
arguments = docopt.docopt(__doc__)
filename = arguments['<filename>']
if filename==None: filename='eventCounts.csv'
					
## READ FROM CSV FILE ##	
with open(filename, 'r') as csvfile:
	reader = csv.reader(csvfile)
	i=0
	for row in reader:
		if i%2==0: 		#ignore every second row (empty)
			EVENT_TIME_COUNTS[int(i/2)] = int(row[0])
		i+=1
csvfile.close()

import plotly
from plotly.graph_objs import Scatter, Layout

plotly.offline.plot({
    "data": [Scatter(x=TIMES, y=EVENT_TIME_COUNTS)],
    "layout": Layout(title="Number of events per hour:	(Recursive search depth: 1)")
})



